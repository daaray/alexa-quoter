import logging
import os
import random

import requests

from flask import Flask, render_template
from flask_ask import Ask, question, statement

from tenacity import RetryError, retry, stop_after_delay


app = Flask(__name__)
ask = Ask(app, "/")
logging.getLogger('flask_ask').setLevel(logging.DEBUG)

QUOTE_API_URL = 'https://api.forismatic.com/api/1.0/'
PARAMS = {
    'method': 'getQuote',
    'format': 'json',
    'lang': 'en',
    'key': 1
}


def prepare_response(data, msg):
    """
    Ingests a data dictionary and message string.
    Outputs the speech text for Alexa to deliver
    """
    data['quoteAuthor'] = data['quoteAuthor'] if data['quoteAuthor'] else 'Anonymous'  # noqa
    speech_text = '{quoteText} attributed to {quoteAuthor}'.format(**data)
    if msg:
        speech_text = '{} {}'.format(msg, speech_text)
    return speech_text


@retry(stop=stop_after_delay(7))
def match_author(author, params):
    """
    Try to find a quote by the supplied author
    The API does not support querying by auth :(
    """
    found = False
    speech_text = None
    while not found:
        params['key'] = random.randint(1, 999999)
        req = requests.get(QUOTE_API_URL, params=PARAMS)
        if req.status_code == 200:
            try:
                data = req.json()
                if author in data['quoteAuthor']:
                    found = True
                    msg = 'We found a quote by {}, here you go!'.format(author)
                    speech_text = prepare_response(data, msg)
                    return speech_text
            except ValueError as e:
                # TODO deal with certain quotes that cannot be parsed
                pass
        else:
            return 'There was a problem reaching the quote API'
        raise Exception


@retry
def get_quote(params, msg):
    """
    Retrieve a quote from the API
    Retry if the response includes bad JSON
    """
    params['key'] = random.randint(1, 999999)
    req = requests.get(QUOTE_API_URL, params=PARAMS)
    if req.status_code == 200:
        try:
            return prepare_response(req.json(), msg)
        except ValueError as e:
            # TODO deal with certain quotes that cannot be parsed
            pass
    else:
        return 'There was a problem reaching the quote API'


@ask.launch
def launch():
    speech_text = 'You can say "say a quote" to have a random quote read to you. If ' \
        'you would like to here a quote from your favorite speaker or auther, say "quote me" followed ' \
        'by your author\'s name.  Example: Quote me Emerson".'
    return question(speech_text).reprompt(speech_text).simple_card('Quote Engine', speech_text)


@ask.intent('QuoterIntent',
    mapping={'author': 'authorName'},
    default={'author': 'x'}
    )
def quoter(author):
    if author != 'x':
        try:
            speech_text = match_author(author, PARAMS)
        except RetryError:
            # timed out retrying
            msg = 'Sorry, we could not find a quote by {} in time, try this one.'.format(author)
            speech_text = get_quote(PARAMS, msg)
    else:
        msg = 'Here is your quote!'
        speech_text = get_quote(PARAMS, msg)
    return statement(speech_text).simple_card('Quote Engine', speech_text)


@ask.intent('AMAZON.HelpIntent')
def help():
    speech_text = 'You can say "say a quote" to have a random quote read to you. If ' \
        'you would like to here a quote from your favorite speaker or auther, say "quote me" followed ' \
        'by your author\'s name.  Example: Quote me Emerson".'
    return question(speech_text).reprompt(speech_text).simple_card('Quote Engine', speech_text)


@ask.intent('AMAZON.StopIntent')
@ask.intent('AMAZON.CancelIntent')
def cancel():
    speech_text = render_template('stop_quoter')
    return statement(speech_text)


@ask.session_ended
def session_ended():
    return "{}", 200


if __name__ == '__main__':
    if 'ASK_VERIFY_REQUESTS' in os.environ:
        verify = str(os.environ.get('ASK_VERIFY_REQUESTS', '')).lower()
        if verify == 'false':
            app.config['ASK_VERIFY_REQUESTS'] = False
    app.run(debug=True)

